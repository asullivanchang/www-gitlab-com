---
layout: markdown_page
title: "Category Vision - Incident Management"
---

- TOC
{:toc}

## Introduction and how you can help
Thanks for visiting this category page on Incident Management in GitLab. This page belongs to the Health group of the Monitor stage, and is maintained by [Sarah Waldner](https://gitlab.com/sarahwaldner) who can be contacted directly via [email](mailto:swaldner@gitlab.com). This vision is a work in progress and everyone can contribute. Sharing your feedback directly on issues and epics at GitLab.com is the best way to contribute to our vision. If you’re a GitLab user and have direct knowledge of your need for incident management, we’d especially love to hear from you.

## Overview

Downtime costs companies an average of $5,600/minute, [according to Gartner](https://blogs.gartner.com/andrew-lerner/2014/07/16/the-cost-of-downtime/).
This number, though an estimate based on a wide range of companies, communicates that downtime is expensive for organizations.
This is especially true for those who have not invested in culminating process and culture around managing these outages and resolving them quickly.
The larger an organization becomes, the more distributed their systems and teams tend to be.
This distribution leads to longer response times and more money lost for the business.
Investing in the right tools and fostering a culture of autonomy, feedback, quality, and automation leads to more time spent innovating and building software and less time spent reacting to outages and racing to restore services.
The tools your DevOps teams use to respond during incidents critically affect [MTTR (Mean Time To Resolve, also known Mean Time To Repair)](https://en.wikipedia.org/wiki/Mean_time_to_repair) as well as the happiness and moral of team members responsible for the IT services your business depends on.
A robust incident management platform consumes inputs from all sources, transforms those inputs into actionable incidents, routes them to the responsible party, and then empowers the response team to quickly understand and remediate the problem at hand.

### Mission
Our mission is to empower DevOps teams by automating the creation of rich, relevant incidents, enabling collaboration on multiple communication platforms, and supporting continuous improvement via Post Incident Reviews and system recommendations.

### Challenges
As we invest R&D in building out Incident Management at GitLab, we are faced with the following challenges:
* The market is dominated by Incident Management companies that have been around for longer. Specific examples include:
   * [ServiceNOW](https://www.servicenow.com/) - founded in 2003
   * [PagerDuty](https://www.pagerduty.com/) - founded in 2009
   * [Splunk VictorOps](https://victorops.com/) - founded in 2012
   * [Atlassian Opsgenie](https://www.opsgenie.com/) - founded in 2012
* We lack brand identification with Enterprise Ops buyers (also mentioned on the [Ops Vision page](https://about.gitlab.com/direction/ops/#challenges))
* Customers are not going to purchase GitLab for the Incident Management product alone because it is dependent upon many other GitLab features.
* Incident Management features depend upon other open-source tools ([Prometheus](https://prometheus.io/), [Grafana](https://grafana.com/), [Sentry](https://sentry.io/)) that GitLab integrates with to provide much of the functionality in the Monitor stage. Customers not using these tools will not get full value out of Incident Management.

### Opportunities
We are uniquely positioned to take advantage of the following opportunities:
* Colocation of code and incidents significantly reduces context switching and accelerates [MTTR](https://en.wikipedia.org/wiki/Mean_time_to_repair). We are easily able to correlate development events such as merge requests and deploys with incidents, shortening the time it takes to find the root cause
* We are well-practiced in building [boring solutions](/handbook/values/#boring-solutions) and [iteration](/handbook/values/#iteration). This will enable us to quickly produce a simple version of Incident Management "just-good-enough" to displace overly complicated existing solutions, while rapidly iterating over the long term towards a lovable product in this category.
* We can repurpose many existing features within GitLab when we design workflows for Incident management. This will enable us to achieve:
   * Accelerated time to market
   * Quick iterations
   * Faster feature adoption we are building on known workflows and concepts
   * Improvements to existing features will benefit a wider set of use cases beyond Incident Management


### High-level Design
#### Incidents in GitLab
We are leveraging GitLab's existing Issue features as a base for Incident Management. In its simplest form, an **Incident** should be the single source of truth (SSOT) for understanding:

* The current state of the incident
* The communication channels where an incident is being worked (Zoom, Slack, etc.)
* Relevant environment changes such as commits, merge requests, code, releases
* Monitoring artifacts such as alerts, errors, metrics, traces, logs
* Annotations such as runbooks and chart visualizations

#### Leveraging Existing Features

**Incidents** will be based on GitLab issues, as mentioned above. This allows us to take advantage of the following features, accelerating how quickly can get software into the hands of customers for feedback:
* [Issue boards](https://docs.gitlab.com/ee/user/project/issue_board.html) can be used to triage and organize incidents
* [GitLab Flavoured Markdown (GFM)](https://docs.gitlab.com/ee/user/markdown.html#gitlab-flavored-markdown-gfm) and [issue templates](https://docs.gitlab.com/ee/user/project/description_templates.html#description-templates) allow users to open customized incidents and automatically assign them to the right team or label them to appear in the correct triage list
* [ChatOps](https://docs.gitlab.com/ee/ci/chatops/README.html#gitlab-chatops) sends issue events to Slack using the [Slack notifications service](https://docs.gitlab.com/ee/user/project/integrations/slack.html#slack-notifications-service) and users can make changes to issues from Slack using [slash commands](https://docs.gitlab.com/ee/user/project/integrations/slack_slash_commands.html)

Even though we are taking advantage of existing features to launch Incident Management, that does not mean we are not investing in new functionality. Read on to find out what we have planned for the future and what is up next.

## Target Audience and Experience
While the Incident Management product category matures through minimal and viable, we are creating an intuitive and streamlined experience for the Operations engineer, DevOps engineer, and Developer. The features we've prioritized are oriented towards getting the right person, the right information to enable them to restore the services they are responsible for as quickly as possible. As Incident Management progresses, we will turn our focus towards features that mobilize larger, distributed DevOps teams and eventually features that provide executive management and business stakeholders insight into holistic system health and status updates on current outages.

## Strategy
### Maturity Plan
We are currently working on maturing **Incident Management** from `minimal` to `viable` and we are targetting the end of FY20 Q3. Definitions of these maturity levels can be found on [GitLab's Maturity page](https://about.gitlab.com/direction/maturity/). The following epics group the functionality we have planned to mature Incident Management.
* [Viable](https://gitlab.com/groups/gitlab-org/-/epics/1493) (IN-PROGRESS)
* [Lovable](https://gitlab.com/groups/gitlab-org/-/epics/1494)
* [Complete](https://gitlab.com/groups/gitlab-org/-/epics/1539)

### What is Next & Why?
Collaboration with teammates and rich, relevant, and well-organized incidents accelerate the fire-fight by enabling efficient knowledge sharing, providing guidelines for resolution, and minimizing the number of tools you need to check before finding the problem. These are the table-stakes of Incident Management and the functionality that will make this product category `viable`.

#### Focus Areas
We are immediately focused on the following functionality for maturing Incident Management to `viable`:
* Integrations with collaboration tools
   * [Slack](https://gitlab.com/groups/gitlab-org/-/epics/1524)
   * [Zoom](https://gitlab.com/groups/gitlab-org/-/epics/1439)
* Embedding observability metrics in incidents
   * [GitLab metrics](https://gitlab.com/groups/gitlab-org/-/epics/1434)
   * [Grafana](https://gitlab.com/groups/gitlab-org/-/epics/1631)
* Building an [alert endpoint](https://gitlab.com/groups/gitlab-org/-/epics/1682) that will enable GitLab to consume alerts from a variety of monitoring tools.

#### Use Cases
We recognize that the `viable` version of Incident Management will not work for everyone. We have been strategic in the functionality that we prioritized for this maturity up-level, targetting customers in our `Ultimate` tier who currently align with our Single Application for the DevOps Life-cylc vision. **Incident management** will be most successful for customers who are:

* `Ultimate` - At the time of this writing (August 2019), alerting is only available for this tier
* Using GitLab to host code for external services that they need to keep available - it will be challenging for teams who maintain GLaaS (GitLab as a Service) for their companies to use GitLab for incident management in the scenario that Gitlab goes down
* Using the following technologies:
   * Prometheus
   * Slack
   * Zoom

### Beyond Viable
Once we achieve `viable` for Incident Management, we will be pursuing the following:

* Creating a [Post Incident Review experience](https://gitlab.com/groups/gitlab-org/-/epics/1782) that empowers DevOps teams to continuously improve behavior and systems

* [Linking incident response runbooks](https://gitlab.com/groups/gitlab-org/-/epics/1436) to incidents to help the on-call responder reduce MTTR

* Improving how teams [triage and organize incidents](https://gitlab.com/groups/gitlab-org/-/epics/1435) ensuring multi-problem outages have awareness and are being addressed

* [Integrating with widely used paging, workflow, and ticketing tools](https://gitlab.com/groups/gitlab-org/-/epics/1438) to eliminate manual work required to update multiple systems

### What is not planned right now

These features are currently out of scope for Incident Management and are not planned for any maturity levels at this time. This does not exclude them from future considerations.

* Paging
* On-call schedules
* Escalation
* Remediation

## Competitive Landscape
[Atlassian OpsGenie](https://www.opsgenie.com/)  
[Splunk VictorOps](https://victorops.com/)  
[PagerDuty](https://www.pagerduty.com/)  
[ServiceNOW](https://www.servicenow.com/products/incident-management.html)  
[XMatters](https://www.xmatters.com/use-cases/major-incident-management-mim/)  

## Analyst Landscape
Not yet, but accepting merge requests to this document.

## Top Customer Success/Sales Issue(s)
Not yet, but accepting merge requests to this document.

## Top Customer Issue(s)
Not yet, but accepting merge requests to this document.

## Top Internal Customer Issue(s)
Not yet, but accepting merge requests to this document.

## Top Vision Item(s)
Not yet, but accepting merge requests to this document.
