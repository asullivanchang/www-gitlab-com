---
layout: markdown_page
title: "SDM.1.01 - System Documentation Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# SDM.1.01 - System Documentation

## Control Statement

Documentation of system boundaries and key aspects of their functionality are published to authorized personnel.

## Context

This control formalizes the idea that we need to keep track of our production systems and maintain quality documentation for easy reference. Most of this control is naturally met by the emphasis we have on documentation here at GitLab. The remainder of this control is meant to ensure we are publishing any institutional knowledge about how systems interact and that we consider high-level system views as well as individual components.

## Scope

This control applies to all GitLab production systems.

## Ownership

TBD

## Implementation Guidance

For detailed implementation guidance relevant to GitLab team-members, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/SDM.1.01_system_documentation.md).

## Reference Links

For all reference links relevant to this control, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/SDM.1.01_system_documentation.md).

## Examples of evidence an auditor might request to satisfy this control

For examples of evidence an auditor might request, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/SDM.1.01_system_documentation.md).

## Framework Mapping

* SOC2 CC
  * CC2.3
