---
layout: markdown_page
title: "Tools - Rubocop"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# Rubocop

rubocop is used for running static code analysis on your ruby code.

### "Magic" one-liners

#### rubocop-ing a set of files

You can combine the tips about listing files on the [`Tools - git` page](/handbook/tools-and-tips/git/#listing-files) with rubocop.

- To rubocop the current commit - `git diff-tree --no-commit-id --name-only -r HEAD | xargs bundle exec rubocop`
- To rubocop the working tree changes - `git diff --name-only | xargs bundle exec rubocop`
- To rubocop all of the changes from the branch - `git diff --name-only master | xargs bundle exec`
