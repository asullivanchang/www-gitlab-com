---
layout: markdown_page
title: "Okta"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## What is Okta?

From the Okta website - Okta is the foundation for secure connections between people and technology. It’s a service that gives employees, customers, and partners secure access to the tools they need to do their most important work.

In practice - Okta is an Identity and Single Sign On solution for applications and Cloud entities. It allows GitLab to consolidate authentication and authorisation to Applications we use daily through a single dashboard and ensure a consistent, secure and auditable login experience for all our staff.


## How is GitLab using Okta?

GitLab is using Okta for a few key goals :

- We can use Okta to enable Zero-Trust based authentication controls upon our assets, so that we can allow authorised connections to key assets with a greater degree of certainty.
- We can better manage the login process to the 80+ and growing cloud applications that we use within our tech stack.
- We can better manage the Provisioning and Deprovisioning process for our users to access these application, by use of automation and integration into our HRIS system.
- We can make Trust and Risk based decisions on authentication requirements to key assets, and adapt these to ensure a consistent user experience.


## What are the benefits to me using Okta as a user?

- A single Dashboard that is provided to all users, with all the applications you need in a single place. 
- Managed SSO and Multi-Factor Authentication that learns and adapts to your login patterns, making life simpler to access the assets you need. 
- Transparent Security controls with a friendly User Experience. 

## What are the benefits to me as an application administrator to using Okta?

- Automated Provisioning and Group Management
- Ability to transparently manage shared credentials to web applications without disclosing the credentials to users
- Centralised access for users, making it easy to add, remove and change the application profile without the need to update all users. 

## How do I get my Okta account set up?

All GitLab team-members will have an Okta account set up as part of their onboarding process. 

Existing Gitlab Team Members will have activation emails sent out in June 2019. 

Follow the GitLab Okta [Getting Started Guide](https://docs.google.com/document/d/1x2NJan0job5nM5tT8HF6yofg-Y2aAsSVKc6qNnCuoxo/) and [FAQs](/handbook/business-ops/okta/okta-enduser-faq/).

We have also prepared Introductory Videos on [Okta Setup](https://youtu.be/upJ4p3lKYKw), [Setting up MFA/Yubikeys](https://youtu.be/9UyKml_aO3s), [Configuring Applications](https://youtu.be/xS2CarGUPLc) and [Dashboard Tips](https://youtu.be/xQQwa_pbe2U). 

We recommend particularly that once your account is set up, you set up an additional MFA factor (either YubiKey or Google Authenticator/TOTP) in case there's an issue with one of your MFA factors.

## Setting up my Okta Account requires me to use Okta Verify on my Phone, and I don't like that...

Our Okta implementation defaults to using Okta Verify as the Required MFA factor. Okta Verify is a safe and secure application that allows Push Notifications and One-time tokencodes on your phone to validate your login. It is supported on iPhone, Android and Windows Phones.

For some people, there are issues with installing a verification App on their phone. If there is some reason that this is not appropriate for your geography or other reasons, please submit an issue to [Opt Out](https://gitlab.com/gitlab-com/gl-security/zero-trust/okta/issues/new?issuable_template=okta_verify_optout) and we can add you to an authentication group that will make Okta Verify optional. Please note that we still recommend that you set up at least two MFA factors, in case something happens to one of your factors. 


## Why isn't an Application I need available in Okta?

First up, check against the [Okta Application list](/handbook/business-ops/okta/okta-appstack/) to see if your application is listed there with a correct status. If you cannot see your Application listed here, create a [new application setup issue](https://gitlab.com/gitlab-com/gl-security/zero-trust/okta/issues/new?issuable_template=okta_add_application) and fill in as much information as you can.

## How do I get my Application set up within Okta?

If you are an Application Owner please submit a [new application setup issue](https://gitlab.com/gitlab-com/gl-security/zero-trust/okta/issues/new?issuable_template=okta_add_application) on the Okta project page for your application. We will work with you to verify details and provide setup instructions. 

## I have an Application that uses a Shared Password for my team, can I move this to Okta?

Yes you can! Submit a [new application setup issue](https://gitlab.com/gitlab-com/gl-security/zero-trust/okta/issues/new?issuable_template=okta_add_application) on the Okta project page for your application. We will work with you to verify details and provide setup instructions.

## Where do I go if I have any questions?

- **Slack** `#okta` Channel
- [Okta Comments issue](https://gitlab.com/gitlab-com/gl-security/zero-trust/okta/issues/2)


